--[[
   Copyright: Acadia Creative Commons
   Author: Will Sinatra
]]--

--Require Acadia State Trigger
local aState = require 'states/aState'

--Bind love.load to aState setState to load specific states
function love.load()
   aState.setState("mainMenu")
end

--Trigger update/draw/key events to current state mechanism
function love.update(dt)
   aState.stateEvent("update", dt)
end

function love.draw()
   aState.stateEvent("draw")
end

function love.keyreleased(key, code)
   aState.stateEvent("keyreleased", key, code)
end

function love.quit()
   print("Exiting")
end
