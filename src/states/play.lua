--[[
   Copyright: Acadia Creative Commons
   Author: Will Sinatra
]]--

local play = {}
local aState = require 'states/aState'
local sti = require 'lib/sti'
playState = nil

function play.info()
   love.graphics.print("This would give some info stuff")
end

function play.progress()
   love.graphics.print("This would progress to the next turn")
end

function play.load()
   font = love.graphics.newFont(32)
   map = sti("assets/modern_island.lua")
end

function play.update(dt)
end

function play.draw()
   --Set window width/height dynamically
   local ww = love.graphics.getWidth() / 2
   local wh = love.graphics.getHeight() / 2
   map:drawLayer(map.layers[1])
   
   if playState == 'info' then
   	  play.info()
   end
   
   if playState == 'progress' then
   	  play.progress()
   end
end

function play.keyreleased(key, code)
   --If ESC is pressed, load pause menu, which provides save exit resume options
   if key == 'escape' then
      aState.setState("pause", { curState = "pause" })
   end
   
   --If P is pressed, progress to next turn
   if key == 'p' then
      playState = "progress"
   end

   --If I is pressed, print info regarding piece (this should be combined with a mouse hovering
   if key == 'i' then
   	  playState = "info"
   end
end

return play
