--[[
   Copyright: Acadia Creative Commons
   Author: Will Sinatra
]]--

local loadMenu = {}
local aState = require 'states/aState'

--Button definition table
local function newButton(text, fn)
   return {
      text = text,
      fn = fn,
      now = false,
      last = false
   }
end

--Table of buttons containing button definition tables
local buttons = {}
local font = nil

--Create loadMenu buttons on load, triggered by aState.stateEvent("loadMenu")
function loadMenu.load()
   font = love.graphics.newFont(32)

   table.insert(buttons, newButton("Start",
				   function()
				      print("Starting Game")
				      aState.setState("play", { curState = "play" })
   end))

   table.insert(buttons, newButton("Load",
				   function()
				   aState.setState("loadMenu", { curState = "loadMenu" })
   end))

   table.insert(buttons, newButton("Settings",
				   function()
				      print("Setting Settings")
   end))

   table.insert(buttons, newButton("Exit",
				   function()
				      print("Exiting Acadia")
				      love.event.quit()
   end))
end

function loadMenu.update(dt)
end

--Draw buttons love callback
function loadMenu.draw()
   --Set window width/height dynamically
   local ww = love.graphics.getWidth()
   local wh = love.graphics.getHeight()

   --Button dimension definitions
   local buttonHeight = 64
   local buttonWidth = ww * (1/3)
   local margin = 16
   local cursorY = 0

   local totalHeight = (buttonHeight + margin) * #buttons

   --Generate buttons, for integer pair in buttons table { 1 = "Start" } do..
   for i, button in ipairs(buttons) do
      --Set last updated to now value
      button.last = button.now

      --Define vector positions of buttons
      local bx = (ww * 0.5) - (buttonWidth * 0.5)
      local by = (wh * 0.5) - (totalHeight * 0.5) + cursorY

      --Set button color
      local color = {0.4, 0.4, 0.5, 1.0}

      --Define mouse position vector as love callback
      local mx, my = love.mouse.getPosition()

      --Define hot (if button should be highlighted) as the mouse vector being inside of the bounds of the buttons position
      local hot = mx > bx and mx < bx + buttonWidth and
	 my > by and my < by + buttonHeight

      --If hot, make the button brighter
      if hot then
	 color = {0.8, 0.8, 0.9, 1.0}
      end

      --If button is pressed, and wasn't triggered before the love.load call resets it to false, trigger the button.fn() definition in the button table
      button.now = love.mouse.isDown(1)
      if button.now and not button.last and hot then
	 button.fn()
      end

      --Set text color, unpack from color table (note we have a definition of this above for the buttons themselves which we ignored calling because I prefer the text on a plain screen for now, but we can add this same call after local color = {0.4 ... to make the button boxes show correctly.)
      love.graphics.setColor(unpack(color))

      --Get width/height of generated text dynamically based on value of button table text definition
      local textW = font:getWidth(button.text)
      local textH = font:getHeight(button.text)

      --Love print callback to render button text in the center of the button boundaries
      love.graphics.print(
	 button.text,
	 font,
	 (ww * 0.5) - textW * 0.5,
	 by + textH * 0.5)

      --Track position of mouse Y coordinate to generate a cursor and emulate button highlighting
      cursorY = cursorY + (buttonHeight + margin)
   end
end

--Quit main menu if ESC is pressed
function loadMenu.keyreleased(key, code)
   if key == 'escape' then
      love.event.quit()
   end
end

return loadMenu
