--[[
   Copyright: Acadia Creative Commons
   Author: Will Sinatra
]]--

local pause = {}
local aState = require 'states/aState'

local priorObjs = {}

--This needs to be fixed so that we can keep the state of the game before the pause is started
--function pause.enter(priorState)
--   priorObjs = ...
--end

function pause.update(dt)
end

function pause.draw()
   for _, obj in pairs(priorObjs) do
      if type(obj) == "table" and obj.draw then
	 obj.draw()
      end
   end

   love.graphics.print(
      "PAUSED: to continue press Enter, to quit press Esc.", 50, 50)
end

function pause.keyreleased(key, code)
   if key == "return" then
      aState.setState("play")
   elseif key == 'escape' then
      love.event.quit()
   end
end

function pause.exit()
   priorObjs = nil
end

return pause
