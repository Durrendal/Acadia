--[[
   Copyright: Acadia Creative Commons
   Author: Will Sinatra
]]--

local aState = {}

local loaded = {}
local curState = {}

local function getKey(tbl, val)
   for k,v in pairs(tbl) do
      if v == value then
	 return k
      end
   end
   return nil
end

--Set current state into metatable
function aState.setState(stateName, ...)
   aState.stateEvent('exit')
   local priorState = getKey(loaded, curState)
   curState = loaded[stateName]
   if not curState then
      curState = require("states/" .. stateName)
      loaded[stateName] = curState
      aState.stateEvent('load', priorState, ...)
   end
   print(priorState)
   aState.stateEvent('enter', priorState, ...)
end

--Trigger love.X based on curState table info.
--IE: mainMenu.update = love.update trigger. mainMenu.draw = love.draw trigger.
--Each state is locally encapsulated, and contains all love callbacks to properly render to screen/process events.
function aState.stateEvent(funcName, ...)
   if curState and type(curState[funcName]) == 'function' then
      curState[funcName](...)
   end
end

return aState
