local aState = {}

aState.defs = {
   "Menu" = 'states/menu',
   "Pause" = 'states/pause',
   "Combat" = 'states/combat',
   "Inventory" = 'states/inventory',
   "CE" = 'states/currentEpoch',
   "E6" = 'states/epochSix',
   "E5" = 'states/epochFive',
   "E4" = 'states/epochFour',
   "E3" = 'states/epochThree',
   "E2" = 'states/epochTwo',
   "E1" = 'states/epochOne',
   "ENIL" = 'states/epochNil'
}

function aState.loadState(state)
   local curState = require(aState.defs[state])
   return curState
end

return aState
