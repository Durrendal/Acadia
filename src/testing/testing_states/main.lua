local aState = require 'aState'

runningState = aState.defs["Menu"]

function love.state()
   local curState = aState.loadState(runningState)
end

function love.update(dt)
end

function love.draw()
end
