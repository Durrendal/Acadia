--[[ 
   Copyright Acadia Creative Commons
   Authors: Will Sinatra, Samantha Scott, Kenneth Faircloth 
]]--

Acadia = class('Acadia'):include(Stateful)

require 'states/menu'
require 'states/pause'
require 'states/end'

function Acadia:initialize()
   self:gotoState('Menu')
end

function Acadia:exit()
end

function Acadia:update(dt)
end

function Acadia:draw()
end

function Acadia:keypressed(key, code)
   if key == 'p' then
      self:pushState('Pause')
   end
end

function Acadia:mousepressed(x, y, button, isTouch)
end

