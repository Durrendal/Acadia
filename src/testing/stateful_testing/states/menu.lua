local Menu = Acadia:addState('Menu')

local function newButton(text, fn)
   return {
      text = text,
      fn = fn,
      now = false,
      last = false
   }
end

local buttons = {}
local font = nil

function Menu:enteredState()
end

function Menu:load()
   font = love.graphics.newFont(32)
   
   table.insert(buttons, newButton("Start Game",
				   function()
				      print("Starting Game")
   end))

   table.insert(buttons, newButton("Load Game",
				   function()
				      print("Loading Game")
   end))

   table.insert(buttons, newButton("Settings",
				   function()
				      print("Loading Settings Menu")
   end))

   table.insert(buttons, newButton("Exit",
				   function()
				      love.event.quit(0)
   end))
end

function Menu:update(dt)
end

function Menu:draw()
   local ww = love.graphics.getWidth()
   local wh = love.graphics.getHeight()

   local buttonHeight = 64
   local buttonWidth = ww * (1/3)
   local margin = 16
   local cursorY = 0

   local totalHeight = (buttonHeight + margin) * #buttons
   
   for i, button in ipairs(buttons) do
      button.last = button.now
      
      local bx = (ww * 0.5) - (buttonWidth * 0.5)
      local by = (wh * 0.5) - (totalHeight * 0.5) + cursorY

      local color = {0.4, 0.4, 0.5, 1.0}

      local mx, my = love.mouse.getPosition()

      local hot = mx > bx and mx < bx + buttonWidth and
	 my > by and my < by + buttonHeight

      if hot then
	 color = {0.8, 0.8, 0.9, 1.0}
      end

      button.now = love.mouse.isDown(1)
      if button.now and not button.last and hot then
	 button.fn()
      end
      
      love.graphics.setColor(unpack(color))
      love.graphics.rectangle("fill",
			      bx,
			      by,
			      buttonWidth,
			      buttonHeight)

      love.graphics.setColor(0, 0, 0, 1)

      local textW = font:getWidth(button.text)
      local textH = font:getHeight(button.text)
      
      love.graphics.print(
	 button.text,
	 font,
	 (ww * 0.5) - textW * 0.5,
	 by + textH * 0.5)
	 
      
      cursorY = cursorY + (buttonHeight + margin)
   end
end

function Menu:keypressed(key, code)
end
