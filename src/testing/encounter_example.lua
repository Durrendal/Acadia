#!/usr/bin/lua

ticker = 0
state = "testing"

function randomEncounter(ent)
   local encounterTbl = {
      "rat",
      "dog",
      "cat",
      "bunny",
      "gojira"
   }

   if ent == 10 then
      print(encounterTbl[1])
   end

   if ent == 12 then
      print(encounterTbl[2])
   end

   if ent == 16 then
      print(encounterTbl[3])
   end

   if ent == 20 then
      print(encounterTbl[4])
   end

   if ent == 30 then
      print(encounterTbl[5])
   end
end

function doLogic()
   ticker = ticker + 1

   if ticker == 31 then
      state = "done"
      ticker = 0
   end

   randomEncounter(ticker)
end

function playerAct()
   print("Step 1 block")
end

while state == "testing" do
   playerAct()
   doLogic()
end
