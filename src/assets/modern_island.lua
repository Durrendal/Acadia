return {
  version = "1.4",
  luaversion = "5.1",
  tiledversion = "1.4.3",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 32,
  height = 32,
  tilewidth = 32,
  tileheight = 32,
  nextlayerid = 2,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "revolution",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      columns = 16,
      image = "revolution.png",
      imagewidth = 512,
      imageheight = 512,
      objectalignment = "unspecified",
      tileoffset = {
        x = 0,
        y = 0
      },
      grid = {
        orientation = "orthogonal",
        width = 32,
        height = 32
      },
      properties = {},
      terrains = {},
      tilecount = 256,
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      x = 0,
      y = 0,
      width = 32,
      height = 32,
      id = 1,
      name = "Tile Layer 1",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 80, 80, 62, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 80, 62, 62, 1, 1, 1, 1, 129, 1, 1, 1, 14, 80, 80, 35, 80, 32, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 129, 1, 80, 62, 62, 80, 80, 80, 1, 1, 1, 1, 1, 14, 80, 80, 80, 80, 80, 32, 32, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 80, 80, 80, 80, 80, 80, 80, 72, 73, 74, 75, 80, 80, 80, 80, 80, 80, 32, 32, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 80, 62, 62, 80, 36, 87, 88, 89, 90, 91, 92, 92, 92, 92, 93, 80, 80, 80, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 80, 80, 80, 80, 102, 80, 121, 121, 1, 1, 1, 152, 112, 80, 102, 80, 80, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 57, 107, 112, 112, 117, 116, 116, 118, 80, 1, 1, 1, 1, 1, 1, 152, 112, 102, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 57, 107, 112, 112, 80, 80, 80, 80, 80, 1, 1, 1, 1, 1, 1, 105, 107, 102, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 57, 107, 112, 35, 80, 80, 80, 1, 1, 1, 1, 1, 1, 1, 1, 1, 107, 102, 1, 1, 1, 28, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 57, 107, 108, 80, 62, 80, 80, 1, 1, 1, 1, 1, 1, 1, 1, 136, 112, 102, 1, 1, 1, 44, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 57, 123, 108, 80, 80, 80, 80, 1, 1, 129, 1, 1, 1, 1, 1, 107, 80, 109, 116, 116, 116, 60, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 127, 127, 127, 80, 80, 80, 1, 1, 1, 1, 1, 57, 155, 62, 80, 80, 80, 80, 80, 80, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 80, 80, 1, 1, 1, 57, 155, 62, 62, 80, 80, 62, 80, 80, 80, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 80, 80, 1, 57, 155, 62, 80, 80, 80, 62, 80, 80, 32, 32, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 80, 80, 80, 80, 80, 80, 80, 80, 80, 80, 32, 32, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 152, 112, 112, 112, 80, 80, 80, 80, 80, 80, 32, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 129, 1, 1, 1, 1, 11, 1, 1, 107, 112, 112, 80, 80, 80, 63, 63, 80, 32, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 11, 107, 112, 112, 80, 80, 80, 79, 79, 32, 32, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 11, 11, 11, 1, 152, 112, 108, 80, 80, 80, 80, 62, 32, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 129, 1, 1, 1, 152, 153, 80, 80, 62, 62, 62, 62, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 14, 62, 62, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
      }
    }
  }
}
