# Mil Sim

Switching gears to something doable, less complex, but ultimately more interesting I feel.

A Military Strategy Simulation engine.

## What?
Essentially a top down isometric tactical game, think an old school style board game. Vassal will let you play those sure, but they aren't custom games, it's just images and a server. This would be custom content and strategies.
The goal would be to do a few things

-[] Create a hex grid layout for tactical maps
-[] Create a module library to define pieces, their usage, and scenario information, and intermesh those into the engine
-[] A live server module, in the style of send recieve. Player A will make a move, send it to Player B, and back and forth
-[] A play by email mode, send a custom data file defining played turns to another player via email to be ingested and actioned against

## Gameplay:
Point and click with drop down menus of sorts.
Essentially we'd need to define states for the various functions of the game, but at it's heart it would be move pieces on a board, play out military skirmishes
Assuming we have a play by email method we could define a dumb AI using the same module. Turn 1, do X, turn 2, do Y. It would be primitive, but we could feasibly define basic solo runs that way.
Building in some level of AI logic would be a whole other thing.

We'd also need to have some kind of global table that tracks available pieces and their status, that way the game doesn't fuck up when there's no Piece A because it got blown to hell.

These are all just ideas, I was thinking maybe doing something small, like the Penobscot skirmish, or the bruning of Falmouth would be good basic tests. 